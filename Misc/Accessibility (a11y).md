# Accessibility (a11y)

## Resources

### Google's Udacity Course
Rob Dobson, Alice Boxhall and Michael Wales cover the reach of considerations and how to solve for certain situations.  
[https://www.udacity.com/course/web-accessibility--ud891](https://www.udacity.com/course/web-accessibility--ud891)

### Web-A11y Slack Group
A group led by Marcy Sutton of aXe, a web accessibility testing tool. The group is well attended by some of the top professionals in the industry. A great place to ask if you have a question.  
[https://web-a11y.herokuapp.com/](https://web-a11y.herokuapp.com/)


### A11ycasts by Rob Dobson
As part of a Google initiative, Rob Dobson covers a11y in bite size chunks that are fun and inviting.  
[https://www.youtube.com/watch?v=cOmehxAU_4s&list=PLNYkxOF6rcICWx0C9LVWWVqvHlYJyqw7g](https://www.youtube.com/watch?v=cOmehxAU_4s&list=PLNYkxOF6rcICWx0C9LVWWVqvHlYJyqw7g)

### The A11y Project
Initiated and managed by Dave Rupert. Provides simple guides and articles for understanding a11y in an approachable manner.  
[http://a11yproject.com/](http://a11yproject.com/)

### Practical ARIA Examples
Heydon Pickerings simplifies the often confusing ways to use ARIA to increase the a11y of your web page.  
[http://heydonworks.com/practical_aria_examples/](http://heydonworks.com/practical_aria_examples/)

### Inclusive Design Patterns
Book by Heydon Pickerings on the conceptual and technical approaches for building accessible web pages.  
[https://www.dropbox.com/s/lb29yvj2pmyo8g9/inclusive-design-patterns.pdf?dl=1](https://www.dropbox.com/s/lb29yvj2pmyo8g9/inclusive-design-patterns.pdf?dl=1
)

### Maintaining Accessibility in a Responsive Worldwide
[https://www.filamentgroup.com/lab/accessible-responsive.html](https://www.filamentgroup.com/lab/accessible-responsive.html)