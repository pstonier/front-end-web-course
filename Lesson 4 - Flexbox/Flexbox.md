# Flexbox

[CSS-Tricks Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

[Flexbox Frogger](http://flexboxfroggy.com/)

[DevTips Video](https://www.youtube.com/watch?v=G7EIAgfkhmg)

<--!
NOTES
Designing a Product Page Layout with Shopify
Levin Mejia
Shopify
Venture Theme
Code Pen
Defining divs as flex box to affect their position AND behavior.
<section> </section>
"card"
Responsive Flexboxing
-->