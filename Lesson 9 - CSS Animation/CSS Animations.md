# CSS Animations

## `transition`
Example: [https://codepen.io/pstonier/pen/GEqBBJ](https://codepen.io/pstonier/pen/GEqBBJ)

[CSS Transitions Specifications @ CSS-Tricks](https://css-tricks.com/almanac/properties/t/transition/)

## `transform`

[Transform Specifications @ CSS-Tricks](https://css-tricks.com/almanac/properties/t/transform/)

## `animation`

[CSS Animation Specifications @ CSS-Tricks](https://css-tricks.com/almanac/properties/a/animation/)

---

# Greensock (GSAP)

When doing complex animations, most people turn to GSAP — a javascript library for creating animations that are fast and with strong browser support.

- Animating parts of SVGs
- Interactions (When I click X, move Z)
- Multiple Timelines

Examples:

[Wiring Diagram](https://codepen.io/pstonier/pen/rWZvNY)

[Boxes on Hover](https://codepen.io/pstonier/pen/jVxEpR)

[Multiple Timelines](https://codepen.io/pstonier/pen/eBYJjJ)


# Learning Games

[Adventures in Web Animation](http://campus.codeschool.com/courses/adventures-in-web-animations/)

[CSS Diner: Learning CSS Selectors](https://flukeout.github.io/)