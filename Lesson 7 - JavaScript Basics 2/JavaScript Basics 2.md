# Javscript Basics (Continued)

## Agenda
 
- Homework Review: layout of blocks rather than Bob's.
	- Work together to create an ideal layout and build out the page to then use later to manipulate with JavaScript
- Review Video
- Apply learnings from video to the homework page



[JavaScript Fundamentals Video](https://www.youtube.com/watch?v=zPHerhks2Vg)

[CodePen Companion to the JavaScript Fundamentals Video](https://codepen.io/pstonier/pen/BRrqQa)

- Interactions
	- Manipulating elements in the document
	- Methods
		- addEventListener
		- getElementById
		- getElementsByTagName
		- innerHTML
		- classList
	- Properties
		- Indexing
		- length (count number of items)
	- For loops	
	
	
	
## Homework
Follow this page to create a responsive navigation menu following [this guide](https://www.w3schools.com/howto/howto_js_topnav_responsive.asp).