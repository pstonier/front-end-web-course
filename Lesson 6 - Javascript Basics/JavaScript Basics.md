# JavaScript Basics

- Created to add interactive features to webpages
	- Lightboxes
	- Carousels
	- Toggle buttons
	- Tabs
    [Flickity](http://flickity.metafizzy.co/)
    [Flexslider](http://flexslider.woothemes.com/)

- Grown to be very powerful
	- Web Apps
		- Google Maps
		- Gmail
	- Desktop Apps
		- Brackets
	- Server
		- NodeJS

```
Side Note:
- JavaScript is not Java 
```

---

## Vocabulary

### Methods
A command that runs a specific actions — built-in functions of the language. ie `alert();`  
[See Docs](https://docs.microsoft.com/en-us/scripting/javascript/reference/javascript-methods)

### Data Types (values)
Can be a:

- Boolean: True or False
- Number
- String: A single line of text (banana) [See Docs](https://www.w3schools.com/js/js_strings.asp)
- Array: A set of strings (apple, orange, banana)
- `null`, `undefined`, `NaN` (Not a number), Infinity

[More info on Data Types](https://docs.microsoft.com/en-us/scripting/javascript/data-types-javascript)


### Variables
A shorthand used to store a value. ie `var score = 100;`  
[Words you can't use for variables](https://docs.microsoft.com/en-us/scripting/javascript/reference/javascript-reserved-words)

### Statements
Words used to define a situation — such as a conditional statement.  
`if`, `else`, `elseif`, `break`, `switch`   
[See Docs](https://docs.microsoft.com/en-us/scripting/javascript/reference/javascript-statements)

### Loops (Iterations)
A command used to repeat an action.  

- `for` - Uses 3 parameters (starting point, condition to stop when it is false, action to be done at the end of each iteration) [Example](https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_state_for)
- `while` - Repeats until the condition is false. [Example](https://www.w3schools.com/js/js_loop_while.asp)


### Operators
Used for math and logic. ie Greater than or equal to: `>=`   
[See Docs](https://docs.microsoft.com/en-us/scripting/javascript/operators-javascript)




--- 



## Methods


[CodePen Example 1: Alerts](http://codepen.io/pstonier/pen/MpOovP)

[CodePen Example 2: Alerts + Variables](http://codepen.io/pstonier/pen/Zeayow)

[CodePen Example 3: console.log and math](http://codepen.io/pstonier/pen/PpOKRx)

[CodePen Example 4: prompt](http://codepen.io/pstonier/pen/xqPLJr)

[CodePen Example 5: toUpperCase and toLowerCase](http://codepen.io/pstonier/pen/XMzayp)

---


## Where to put scripts

### Inline scripts
```
<html>
  <head>
  </head>
  <body>
    <script>
      // code goes here
    </script>
  </body>
</html>
```

### External scripts
```
<html>
  <head>
    <script src="/assets/js/scripts.js"> </script>
  </head>
  <body>
  </body>
</html>
```


