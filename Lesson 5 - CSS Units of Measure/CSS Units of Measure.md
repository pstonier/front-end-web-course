#CSS Measurement Units

## Absolute Lengths
Does not base their size on anything else.
Pixels are the most common.
- px = pixels  
	[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_px)
- in = inch  
	[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_in)
- cm = centimeter  
	[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_cm)
- mm = millimeters  
	[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_mm)
- pt = points  
	[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_pt)
- pc = picas  
	[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_pc)


## Relative Lengths
Based on the size of other elements:

- Parent's Dimensions (%)
	- example: `width: 50%;` = will take 50% of the width of it's parent
	
- Currently declared font attributes
	- em = based on the M of the current font. The size of the em cascades within the document.  
		[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_em)   
	[See video for example](https://youtu.be/qrduUUdxBSY?t=619)  

	- rem = based on the M of the `:root` font size.  
	[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_rem)    
	[See video about setting up font size for :root](https://youtu.be/UHf3aQz50jQ?t=99)  

	- ex = based on the x-height of the current font  
	[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_ex)
	- ch = based on the width of the 0 of the current font  
	[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_ch)
- Viewport Dimensions
	- vw = wiewport width
		- `width: 50vw;` = 50% of the width of the viewport  
	[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_vw)
	- vh = viewport height
		- `height: 50vh;` = 50% of the height of the window. Can be useful for controlling position to be "above the fold" or before you have to scroll  
	[Tympanus Reference](https://tympanus.net/codrops/css_reference/length/#section_vh)


# Other


## Color Values

- color name (Pink)
- Hexadecimal value (#004280)
- rgb = red, green, blue [HEX to RBG](https://tympanus.net/codrops/css_reference/rgb/)
- rgba = red, green, blue, alpha (opacity)
- hsl = hue, saturation, lightness
- hsla = hue, saturation, lightness, alpha

[Tympanus Reference](https://tympanus.net/codrops/css_reference/color/)

## Resources

[DevTips Video 1: CSS Units](https://www.youtube.com/watch?v=qrduUUdxBSY)

[DevTips VIdeo 2: When to use ems vs rems](https://www.youtube.com/watch?v=UHf3aQz50jQ)