# Lesson 1

012617
## NOTES:::::::::::::::::::::::::NOTES::::::::::::::::::::::::::::NOTES

### Priority 1:
What’s the most important function of the site?
What is your “user story”? What are the most important needs of the user? Goals? What trying to reach so that we may best serve them in the most efficient way possible. Satisfied, not 10 levels. What will be the experience of the user? eg. joe accountant. trying to find info about tax laws. 

### Priority 2:
Contents. Outline of contents. AS written outline and then in rough drafts with images and text as indication of content. 

Mind node as digital site map tool.
- mac only
- mind-mapping
- digital site mapping process

Project digital projector and work on as a group
DynoMapper. Lives in the browser. Sharable. Paid monthly. 14 day trial.

Mind node also indicates parent/child relationships of pages. For large organizations, need to map out to know  what pages exist where.

Design tools: Sketch. Bohemian software. Created exclusively for designing for screens. Similar to Illustrator. 

Adobe (beta software) Experience Design. Free download. Plus, InDesign for layouts.
Plus, prototype.

I want to be “Design Build”
**Issue today:**
Designers pressured  to develop. Developers pressured to design. 
From concept to execution: ideas can be made for real.
Brad Frost: “atomic design”. modularity. start design from smallest element, builds into modules, components, for page layouts. 
Advanced custom fields: article by Paul Stonier.

### Terminology
Front end: “DOM”. document object model. Client side; all the stuff that happens in the view. Presentation of content. Back end build system where information is stored, functioality, management of content.(not affecting what is seen.

#### Mark-up “InDesign”
Mark-down “textEdit” super simplified html really fast to write.

Open Source: Mark Down. pretty darn simple. different syntax.

“separation of concerns”

Anatomy of a HTML tag

search for description, which you put in the meta tag.

`<span>` is like character style  inside paragraph style. </>

Strong, emphasis, bold. All look the same. <b>Span tag for bold</b> present the same. 
Screen reader, strong with inflection. Audio. (accessibility topic)

HTML for email are done as tables. <table> hard to make it look the same in different email clients. mac mail, yahoo, gmail, outlook, … Sparkmail (instead of mac mail). 
App called Sparkmail. Get through things quickly for emails. 

Foundation (like Bootstrap)
Foundation.zurb.com
For writing emails. “Inky”
Foundation.zurb.com/emails.html
Plus, “inliner” makes it more compatible with more email clients. Like, turning type into outlines. 

2005, 2006
New HTML tags of very common use that circumvent CSS. More naturally describes the content.

Uploading an image to wordpress , it sizes it as multiple viewports. “srcset”
WP one of the first to adopt responsive images.

Bitbucket.
GIT repository.
Archives changes.
Multiple people can work on the files. They get merged. 

<meta> STUFF
What does “char set=“UTF-8”> mean? Has to do with language; code behind how language displayed.

SEO: title, description are ket to SEO.

<link rel …>

Value of teaching HTML, CSS with demand for Wix, WordPress, sqspace … skills? Really can’t take true advantage of whats in front of you without knowing basic code of HTML and CSS. 

tympanus.net/codrops/css
Sara Soueidan 
passionate about finding best practices.
NOTES:::::::::::::::::::::::::NOTES::::::::::::::::::::::::::::NOTES

## Terminology
- Front End Development vs Backend Development
	- Client-side
	- Presentation of content
	- What it is not
		- Database design
		- Complex logic
		- Application development
- Markup / Markdown
	- Syntax for writing the content for a document
	- **Markup:** A markup language is a computer language that uses tags to define elements within a document. It is human-readable, meaning markup files contain standard words, rather than typical programming syntax. While several markup languages exist, the two most popular are HTML and XML. [Source](https://techterms.com/definition/markup_language)
	- **Markdown:** a lightweight markup language with plain text formatting syntax designed so that it can be converted to HTML and many other formats using a tool by the same name.[8] Markdown is often used to format readme files, for writing messages in online discussion forums, and to create rich text using a plain text editor. [Source](https://en.wikipedia.org/wiki/Markdown)



---

# HTML (Hypertext Markup Language)

## Anatomy of a HTML Tag

- Element
- Attribute
- Value

```
<element attribute="value">Value</element>
```

### Example:
```
<img src="logo.png" alt="My Logo" class="company-logo" />
```
Element: `img`  
Attributes: `src`, `alt`, `class`  
Values: `logo.png`, `My Logo`, `company logo`

---

## Basic Elements

### HTML
```
<html> </html>
```
This tag wraps the entire HTML document

--

### body
```
<body> </body>
```
This holds the information that will be displayed on the page

--


### head
```
<head> </head>
```
This area holds all the supportive details about the page above the body tag in a HTML tag

--

### link
```
<link />
```
This tag is generally used inside of a `<head>` tag to link to external files such as CSS style sheets or icons images

--

### meta
```
<meta />
```
This tag is generally used inside of a `<head>` tag to provide details about the document such as the author, character set, description, etc.

--

### div (division)
```
<div> </div>
```

--

### p (paragraph)
```
<p> </p>
```

--

### span
```
<span> </span>
```
--

### strong
```
<strong> </strong>
```
--

### emphasis
```
<em> </em>
```
--

### bold (visual without semantic emphasis)
```
<b> </b>
```
--

### italic (visual without semantic emphasis)
```
<i> </i>
```
--

### img (image)
```
<img> </img>
```
[Responsive Images Presentation](http://www.slideshare.net/pstonier/responsive-images-52294073)

---
## Tables

### table
```
<table> </table>
```

--

### tbody
```
<tbody> </tbody>
```

--

### th
```
<th> </th>
```

--

### tr
```
<tr> </tr>
```

--

### td
```
<td> </td>
```

#### Example:
```
<table>
  <tbody>
	  <tr>
	    <th>Month</th>
	    <th>Savings</th>
	  </tr>
	  <tr>
	    <td>January</td>
	    <td>$100</td>
	  </tr>
  </tbody>
</table>
```

---

## Lists

### unordered list (bullets)
```
<ul> </ul>
```
#### Example:
```
<ul>
  <li>Item 1</li>
  <li>Item 2</li>
  <li>Item 3</li>
</ul>
```
--

### ordered list (numbered)
```
<ol> </ol>
```
#### Example:
```
<ol>
  <li>Item 1</li>
  <li>Item 2</li>
  <li>Item 3</li>
</ol>
```
--

### list item
```
<li> </li>
```

--

### Definition List
```
<dl> </dl>
```

--

### Definition Term
```
<dt> </dt>
```

--

### Definition Description
```
<dd> </dd>
```

#### Example
```
<dl>
  <dt>Coffee</dt>
  <dd>Black hot drink</dd>
  <dt>Milk</dt>
  <dd>White cold drink</dd>
</dl>
```

---

## Forms

### form
```
<form> </form>
```

--

### input
```
<input />
```

--

### select
```
<select> </select>
```

--

### option
```
<option />
```

--
### textarea
```
<textarea> </textarea>
```

--
---

## HTML5 Semantic Tags

### article
```
<article> </article>
```

--

### header
```
<header> </header>
```

--

### footer
```
<footer> </footer>
```

--

### aside
```
<aside> </aside>
```

--

### main
```
<main> </main>
```

--

### section
```
<section> </section>
```

--

### video
```
<video> </video>
```

--

### audio
```
<audio> </audio>
```

--

### Figure
```
<figure> </figure>
```
--

### Figure Caption
```
<figcaption> </figcaption>
```