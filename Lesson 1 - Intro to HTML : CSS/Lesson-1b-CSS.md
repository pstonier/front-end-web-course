# CSS

## General Properties

### Background

The `background` property is a shorthand property for setting most background properties at the same place in the style sheet.

For each layer the shorthand first sets the corresponding layer of each of `background-image`, `background-position`, `background-size`, `background-repeat`, `background-origin`, `background-clip` and `background-attachment` to that property’s initial value, then assigns any explicit values specified for this layer in the declaration.  
[https://tympanus.net/codrops/css_reference/background/](https://tympanus.net/codrops/css_reference/background/)

--

### Width

The `width` property is used to set the width of the content box of an element.  
[https://tympanus.net/codrops/css_reference/width/](https://tympanus.net/codrops/css_reference/width/)

--

### Height
The `height` property is used to set the height of the content box of an element.  
[https://tympanus.net/codrops/css_reference/height/](https://tympanus.net/codrops/css_reference/height/)

--

### Minimum Width
The `min-width` property is used to set a minimum width of a specified element.  
[https://tympanus.net/codrops/css_reference/min-width/](https://tympanus.net/codrops/css_reference/min-width/)

--

### Minimum Height
The `min-height` property is used to set a minimum height of a specified element.  
[https://tympanus.net/codrops/css_reference/min-height/](https://tympanus.net/codrops/css_reference/min-height/)

--

### Maximum Width
The `max-width` property is used to set a maximum width of a specified element.  
[https://tympanus.net/codrops/css_reference/max-width/](https://tympanus.net/codrops/css_reference/max-width/)

--

### Maximum Height
The `max-height` property is used to set a maximum height of a specified element.  
[https://tympanus.net/codrops/css_reference/max-height/](https://tympanus.net/codrops/css_reference/max-height/)

--

### Border

The `border` property is a shorthand property used to specify the width, style and color of the top, right, bottom, and left border of a box.  
[https://tympanus.net/codrops/css_reference/border/](https://tympanus.net/codrops/css_reference/border/)

---

## Text Styles

#### Font
The `font` property is a shorthand property for setting longhand properties: `font-style`, `font-variant`, `font-weight`, `font-stretch`, `font-size`, `line-height`, `font-family` at the same place in the style sheet.  
[https://tympanus.net/codrops/css_reference/font/](https://tympanus.net/codrops/css_reference/font/)

--

### Color
The `color` property is used to set the color of an element’s text.
[https://tympanus.net/codrops/css_reference/color/](https://tympanus.net/codrops/css_reference/color/)

---

## Common Approaches

### Images to fit their container
```
img {
	width: 100%;
	height: auto;
}
```



