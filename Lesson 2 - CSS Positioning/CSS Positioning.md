# CSS Positioning
020917
CSS positioning is something that requires a bit of patience and practice to understand what's happening due to all the nuances.

`position: static`   
The default position. Directional statements do not affect the element. The element remains in the normal flow of the page.  
[Code Example: Static](http://codepen.io/pstonier/pen/LxgyVy)

`position: relative`   
Similar to static, but directional statements move the element relative to its position in the flow: IN THE PARENT ELEMENT?.  
[Code Example: Relative](http://codepen.io/pstonier/pen/KaGWMB/)

`position: absolute`  
The element is pulled from the flow and the position of the element does not affect any other elements. The position of the element is set in relation to its nearest ancestor with `position: relative`. If there are no relatively positioned ancestors, the *positioning context* falls back to the window VIEWPORT? AS IF ON A LAYER ABOVE (Z-AXIS)?
THE TEXT IS POSITIONED ABSOLUTE INSIDE THE TEXT BOX THAT IS POSITIONED RELATIVE TO THE WHAT?[See Video](https://youtu.be/Rf6zAP4YnZA?t=68)
[Code Example: Absolute Position](http://codepen.io/pstonier/pen/xgyqvE)  
[Code Example: Absolute Position with Realtive Positioned Container](http://codepen.io/pstonier/pen/pRxPzX)

`position: fixed`  
Positioning is always related to the window and the position persists while the rest of the page scrolls.
[Code Example: Fixed](http://codepen.io/pstonier/pen/NdOjxJ)

`position: sticky`
[See Example](http://codepen.io/simevidas/pen/JbdJRZ)


`z-index`  
CSS Layers. Highest z-index value sits on the top.   
[MDN Reference Examples](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Positioning/Understanding_z_index/Stacking_context_example_1)

## Resources
[MDN Position Spec](https://developer.mozilla.org/en-US/docs/Web/CSS/position)


[CSS Spec Reference: Position](https://tympanus.net/codrops/css_reference/position/)

### Position Statements
[CSS Positioning Part 1 - Static and Relative](https://www.youtube.com/watch?v=kejG8G0dr5U)

[CSS Positioning Part 2 � Absolute and Fixed](https://www.youtube.com/watch?v=Rf6zAP4YnZA)


---


# Display Types
`display: inline` Everything flows like text. [Default for Block Level Elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Block-level_elements)

`display: block` Fills the width by default to stack elements vertically. Works best for floating elements.
[Reference: CSS-Tricks](https://css-tricks.com/almanac/properties/d/display/)

`display: inline-block` Works inline, but can have a defined width and height.

`display: none` Hides the element as if it were removed from the document.


[Video - Display Types](https://www.youtube.com/watch?v=u-3aQpZD3_Q)


# Floats

[CSS Tricks Reference](https://css-tricks.com/all-about-floats/)

[Video](https://www.youtube.com/watch?v=xFGBNv2KeVU)

[Example Code](http://codepen.io/devtips/pen/gbVXMP)