# 20 Week Front End Web Development Course


## Week 1
- **Review Course Overview**
- **HTML**
	- Basic HTML Tags
	- Semantic HTML5 Additions
- **CSS**
	- General Appearance
	- Box Model

- **Homework Assignment:** Create a HTML site with 3 pages (homepage, about, contact) listing 3 articles on the homepage and functioning navigation

-- 

## Week 2
- **CSS**
	- Layout
	- Positioning
	- Display Modes

- **Homework Assignment:** Style the page from Lesson 1 with the 3 articles floating across the page 

--

## Week 3
- **CSS**
	- CSS Selectors
	- Pseudoclasses

- **Homework Assignment:** Review videos from CSS Positioning
 
--

## Week 4	
- **Flexbox**

- **Homework Assignment:** Layout the articles to have equal height and navigation links with equal spacing  
[CSS-Tricks Tutorial](https://css-tricks.com/designing-a-product-page-layout-with-flexbox/)


--

## Week 5
- **CSS Units of Measure**

- **Homework Assignment:** Review video  
[CSS Units Video](https://www.youtube.com/watch?v=qrduUUdxBSY)

--

## Week 6	

- **Intro to JS**
	- Returning Values
		- Variables
		- Math
		- Methods
	- Data types
		- Booleans
		- Integers
		- Strings
		- Arrays

- **Homework Assignment:** Make a responsive page
Page should include:
	- Header with a logo and a navigation
	- A project description
	- 9 Bob photos with captions using the cards created on the flexbox lesson
	- Footer with address and phone number	


--

## Week 7	

- **Intro to JS** Continued
	- Interactions
		- Manipulating elements in the document
		- Methods
			- addEventListener
			- getElementById
			- getElementsByTagName
			- innerHTML
			- classList
		- Properties
			- Indexing
			- length (count number of items)
		- For loops	
- **Homework Assignment:** TBD	


--

## Week 8

- **Modern Workflow**
	- Intro to Gulp, npm
	- Intro to SCSS
	- PostCSS Tools

- **Separation	of Concerns**
	- HTML/CSS/JS Frameworks
	- Grid Systems

--

## Week 9

- **CSS Animation**
	- Transitions
	- Keyframes

--



## Week 10
- **JavaScript Continued**
 - Conditionals
 - Loops
 - Functions
 - jQuery

- **Homework Assignment:** Create a mobile navigation toggle

--

## Week 11
- **Intro to PHP**
	- Echoing Values
	- Includes
	- Functions
	- PHP / MySQL
	- WordPress Theming

- **Homework Assignment:** Convert site to use includes

--

## Week 12
- **Version Control (git)**
	- What is git?
	- Tools
	- How to use git
		- Commits
		- Branches
		- Merging
	- Local vs Hosted Repositories

- **Homework Assignment:** setup a git repository, push it to a Bitbucket repository, add 'pstonier' as a user.

--

## Week 13
- **System Administration**
	- DNS
	- Registration
	- Hosting
	- Control Panels
	- Databases
	- Local Server

- **Homework Assignment:** Create a local WordPress site 	

--

## Week 14
- **WordPress Development**
	- The Loop
	- Conditionals
	- Advanced Custom Fields
	- Custom Post Types

- **Homework Assignment:** Build out a custom post type of events and display the 5 most recent events
	
--

## Week 15
- **Accessibility**
	- Why/What
	- Standards
	- Tools

- **Progressive Enhancement**
	- What/Why
	- Considerations

- **Homework Assignment:** Review current site for potential issues and report on findings

--

## Week 16
- **Web Performance**
	- Load Time
	- Animation
	- Testing, Troubleshooting

- **Homework Assignment:** Review current site for potential issues in performance

--

## Week 17-20
- Collaborative Development Sessions


------


## Total Overview

### HTML
- Basic HTML Tags
- Semantic HTML5 Additions

### CSS
- General Appearance
- Box Model
- Positioning
- Display modes
- Pseudo classes

### JavaScript
- Returning values
	- Variables
	- Math
	- Conditionals
	- Loops
- Functions
- jQuery

### PHP
- Echoing values
- Includes
- WordPress Theming

### Concepts
- Responsive Design
- Seperation of Concerns
- Progressive Enhancement
- Device Agnostic
- Accessibility (a11y)
- Internationalisation (i17n)
- Modular Design
- Design Patterns
- Tracking and Analytics
- Performance Optimization
- Search Engine Optimization

### System Administrator
- DNS
- Domain Registration
- Control Panels
- Technology Stacks


### Tools
- Modern Workflow
	- Task Runners
		- Preprocessors
	- Version Control (git)