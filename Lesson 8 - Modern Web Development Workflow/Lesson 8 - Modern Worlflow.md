# Modern Development Workflow

## Package Managers
- NPM (Front End)
- Composer (PHP)
- Ruby Gems


## SASS Example

[Sass Example](https://codepen.io/pstonier/pen/RgNdNV)

[Sass Article: Using Bourbon Neat for CSS Grid Structure with Semantic Markup](https://medium.com/@pstonier/using-bourbon-neat-for-css-grid-structure-with-semantic-markup-cd1b13d48dc1)

## What is gulp?

Gulp is tool known as a task runner in which you can assign actions such as compiling Sass files and concatenating JavaScript files. 

```
gulp.task('compileSass', function () {
    return gulp.src('./sass/**/*.scss')
            .pipe(maps.init())
            .pipe(sass({includePaths: require('bourbon').includePaths}))
            .pipe(autoprefixer())
            .pipe(cssnano())
            .pipe(maps.write('./'))
            .pipe(gulp.dest('./'))
            .pipe(reload({stream:true}));
});
```


## Post CSS

### Autoprefixer

[https://autoprefixer.github.io/](https://autoprefixer.github.io/)

### CSSNano

[http://cssnano.co/](http://cssnano.co/)


## Version Control: git

### What is git?
[https://www.liquidlight.co.uk/blog/article/git-for-beginners-an-overview-and-basic-workflow/](https://www.liquidlight.co.uk/blog/article/git-for-beginners-an-overview-and-basic-workflow/
)

### Tools

#### Sourcetree
[https://www.sourcetreeapp.com/](https://www.sourcetreeapp.com/)

## Local to Staging to Production
- Local Development
	- MAMP
	- XAMPP
	- Vagrant
	- Docker

## Staging Server
Usually is the same server as production, but in a different folder. Serves as a shared place to test code in the same environment as production.	


# Homework: 
Review this video [https://www.lynda.com/Web-Design-tutorials/Web-Project-Workflows-Gulp-js-Git-Browserify/154416-2.html](https://www.lynda.com/Web-Design-tutorials/Web-Project-Workflows-Gulp-js-Git-Browserify/154416-2.html)

And this book (ePub)
[https://www.dropbox.com/sh/kz496f8dr0q4fuc/AABV-AGanGNv5EPRRiPO5OI2a?dl=1](https://www.dropbox.com/sh/kz496f8dr0q4fuc/AABV-AGanGNv5EPRRiPO5OI2a?dl=1)